FROM iron/ruby
MAINTAINER raghu.nospam@gmail.com

# Create working directory
RUN mkdir /usr/share/blog
WORKDIR /usr/share/blog
RUN echo "https://mirror.csclub.uwaterloo.ca/alpine/v3.7/main" >/etc/apk/repositories \
    && echo "https://mirror.csclub.uwaterloo.ca/alpine/v3.7/community" >>/etc/apk/repositories
RUN apk update  \
	&& apk add git curl bash zip nodejs ca-certificates \
	&& update-ca-certificates \
	&& rm -rf /var/cache/apk/* \
	&& gem install --no-ri --no-rdoc asciidoctor \
	&& mv /usr/bin/asciidoctor /usr/bin/asciidoctor-orig

# Download and install hugo
ARG HUGO_VERSION=0.55.5
ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.tar.gz

RUN curl -fsSL https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} -o /tmp/hugo.tar.gz \
 	&& tar -xvzf /tmp/hugo.tar.gz -C /tmp/ \
	&& mv /tmp/hugo /usr/bin/hugo \
	&& rm -rf /tmp/hugo*

ADD asciidoctor /usr/bin/asciidoctor
RUN chmod a+x /usr/bin/asciidoctor

# Expose default hugo port
EXPOSE 1313

# Automatically build site
ENV HUGO_BASE_URL http://localhost:1313/
CMD hugo server -b ${HUGO_BASE_URL} --bind=0.0.0.0 -ws .
