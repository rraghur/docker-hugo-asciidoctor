#! /bin/bash

if [ "$1" == 'push' ]; then
    echo push
    docker push rraghur/hugo-asciidoctor:v$2-alpine
else
    echo build
    docker build -t rraghur/hugo-asciidoctor:v$1-alpine --build-arg HUGO_VERSION=$1 .
fi
